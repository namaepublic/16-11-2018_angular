﻿using UnityEngine;

public class MoveableBoardController : MonoBehaviour
{
    [SerializeField] private Camera _camera;
    [SerializeField] private Transform _moveableBoardTransform;

    private void OnValidate()
    {
        _camera = FindObjectOfType<Camera>();
    }

    private void OnMouseDrag()
    {
        var boardPos = _camera.WorldToScreenPoint(_moveableBoardTransform.position);
        var pos = Input.mousePosition - boardPos;
        var angle = Mathf.Atan2(pos.y, pos.x) * Mathf.Rad2Deg;
        _moveableBoardTransform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
}