﻿using UnityEngine;

public class LevelsController : MonoBehaviour
{
    [SerializeField] private GameController _gameController;
    [SerializeField] private BallController _ballController;
    [SerializeField] private LevelController[] _levels;

    private int _level;

#if UNITY_EDITOR
    private void OnValidate()
    {
        _gameController = FindObjectOfType<GameController>();
        _ballController = FindObjectOfType<BallController>();
        _levels = GetComponentsInChildren<LevelController>(true);
    }
#endif

    public void Init()
    {
        _level = 0;
        _ballController.Stop();
        foreach (var levelController in _levels)
            levelController.gameObject.SetActive(false);
    }

    public void NextLevel()
    {
        if (_level >= _levels.Length)
        {
            _gameController.GameOver();
            return;
        }

        foreach (var levelController in _levels)
            levelController.gameObject.SetActive(false);

        _levels[_level].StartLevel();
        _level++;
    }

    public void RestartLevel()
    {
        _level--;
        NextLevel();
    }
}