﻿using System.Collections;
using UnityEngine;

public class BallController : MonoBehaviour
{
    [SerializeField] private Rigidbody2D _rigidbody2D;
    [SerializeField] [HideInInspector] private int _targetLayer, _deadZoneLayer;

    [SerializeField] private LevelsController _levelsController;
    private bool _init;

    private void OnValidate()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _targetLayer = LayerMask.NameToLayer("Target");
        _deadZoneLayer = LayerMask.NameToLayer("DeadZone");
        _levelsController = FindObjectOfType<LevelsController>();
    }

    public void Init(Vector2 position)
    {
        _rigidbody2D.position = position;
        _rigidbody2D.isKinematic = false;
        
        StartCoroutine(InitAfterFrame());
    }

    private IEnumerator InitAfterFrame()
    {
        yield return new WaitForEndOfFrame();
        _init = true;
    }

    public void Stop()
    {
        _init = false;
        _rigidbody2D.velocity = Vector2.zero;
        _rigidbody2D.angularVelocity = 0;
        _rigidbody2D.isKinematic = true;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!_init) return;
        
        if (other.gameObject.layer == _targetLayer)
        {
            Stop();
            _levelsController.NextLevel();
            return;
        }

        if (other.gameObject.layer != _deadZoneLayer) return;
        
        Stop();
        _levelsController.RestartLevel();
    }
}
