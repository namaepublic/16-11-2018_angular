﻿using System.Linq;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    [SerializeField] private Transform _spawn;
    [SerializeField] private BallController _ballController;

#if UNITY_EDITOR
    private void OnValidate()
    {
        _spawn = GetComponentInChildrenWithTag<Transform>(this, "Respawn");
        _ballController = FindObjectOfType<BallController>();
    }
#endif

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(_spawn.position, .3f);
    }

    public void StartLevel()
    {
        gameObject.SetActive(true);
        _ballController.Init(_spawn.position);
    }

#if UNITY_EDITOR
    public static T GetComponentInChildrenWithTag<T>(Component component, string tag, bool includeInactive = false)
        where T : Component
    {
        return component.GetComponentsInChildren<T>(includeInactive).FirstOrDefault(item => item.CompareTag(tag));
    }
#endif
}