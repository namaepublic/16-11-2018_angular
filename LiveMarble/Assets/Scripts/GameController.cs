﻿using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    [SerializeField] private LevelsController _levelsController;

    private void OnValidate()
    {
        _levelsController = FindObjectOfType<LevelsController>();
    }

    private void Start()
    {
        Init();
    }

    private void Init()
    {
        _levelsController.Init();
        StartGame();
    }

    private void StartGame()
    {
        _levelsController.NextLevel();
    }

    public void GameOver()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    
    public void Quit()
    {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}